/****************************************
 * Copyright (c) 2020 Dedi Hirschfeld
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ****************************************/

//!
/// A directory-scanner thread code.
///
use std::error::Error;
use std::fmt;
use std::fs;
use std::io;
use std::path::PathBuf;
use std::sync::mpsc;

/// A command message from manager to scanner thread.
pub enum Command {
    Scan(PathBuf), // Scan a specific directory.
    Done,          // You can terminate now.
}

#[derive(Debug)]
pub struct ScanError {
    msg: String,
}

impl fmt::Display for ScanError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl Error for ScanError {}

impl ScanError {
    pub fn new(msg: String) -> ScanError {
        ScanError { msg: msg }
    }

    pub fn from_io_error(error: io::Error, filename: String) -> ScanError {
        ScanError::new(format!("{}: {}", filename, error.to_string()))
    }
}

/// A notification message from scanner thread to manager.
pub enum Notification {
    Ready(usize),       // Ready to accept jobs. Arg is the thread ID.
    FoundFile(PathBuf), // got a file.
    FoundDir(PathBuf),  // got a directory.
    Error(ScanError),   // An error occured.
}

/// Send an eror notification on the given channel.
fn notify_error(err: ScanError, tx: &mpsc::Sender<Notification>) {
    // TODO: Handle unwrap.
    tx.send(Notification::Error(err)).unwrap();
}

/// Actual scan code - scan a folder, raising notification about found
/// files/directories and errors.
/// This function
fn do_scan(
    dir: PathBuf,
    tx: &mpsc::Sender<Notification>,
    follow_links: bool,
) -> Result<(), ScanError> {
    let dir_name = dir.to_string_lossy().into_owned();
    let dir_entries = match fs::read_dir(dir) {
        Ok(dir_entries) => dir_entries,
        Err(error) => {
            notify_error(ScanError::from_io_error(error, dir_name.clone()), tx);
            return Ok(());
        }
    };

    for entry in dir_entries {
        let entry = match entry {
            Ok(entry) => entry,
            Err(error) => {
                notify_error(ScanError::from_io_error(error, dir_name.clone()), tx);
                continue;
            }
        };

        let metadata = if follow_links {
            fs::metadata(entry.path().as_path())
        } else {
            fs::symlink_metadata(entry.path().as_path())
        };
        let metadata = match metadata {
            Ok(metadata) => metadata,
            Err(error) => {
                let file_name = entry.path().to_string_lossy().into_owned();
                notify_error(ScanError::from_io_error(error, file_name), tx);
                continue;
            }
        };
        let is_dir = metadata.is_dir();

        if is_dir {
            tx.send(Notification::FoundDir(entry.path())).unwrap();
        } else {
            tx.send(Notification::FoundFile(entry.path())).unwrap();
        }
    }

    Ok(())
}

/// The scanner thread main body.  Scanner threads accept 'scan'
/// messages from manager, and non-recursively scans the corresponding
/// directory, sending responses back to it.
pub fn run(
    thread_id: usize,
    incoming: mpsc::Receiver<Command>,
    outgoing: mpsc::Sender<Notification>,
    follow_links: bool,
) -> () {
    loop {
        outgoing.send(Notification::Ready(thread_id)).unwrap();
        match incoming.recv() {
            Ok(Command::Scan(dir)) => {
                if let Err(err) = do_scan(dir, &outgoing, follow_links) {
                    notify_error(err, &outgoing);
                }
            }
            Ok(Command::Done) => {
                break;
            }
            Err(err) => {
                // Receiver dead, nothing much we can do.
                eprintln!("Error communication with manager thread: {}", err);
                break;
            }
        }
    }
}
