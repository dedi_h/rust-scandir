/****************************************
 * Copyright (c) 2020 Dedi Hirschfeld
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ****************************************/

use std::env;
use std::process;

use clap::{App, Arg};

fn main() {
    let mut args = env::args();
    let app_name = args.next().unwrap();

    let mut app = App::new(app_name)
        .version("0.1")
        .about("Scan a directory recursively")
        .arg(Arg::with_name("follow-links")
             .short("l")
             .long("follow-links")
             .help("Follow directory symbolic links"))
        .arg(Arg::with_name("dir")
             .multiple(true)
             .help("Directory to scan")
        );
    let args = app.clone().get_matches();

    let follow_links = args.index_of("follow-links").is_some();

    let dirs_to_scan: Vec<String> = match args.values_of("dir")
    {
        Some(iter) => iter.map(|s| String::from(s)).collect(),
        None => {
            app.print_help().unwrap_or_else(|e| e.exit());
            println!("");
            process::exit(-1);
        }
    };
    if !dirs_to_scan.is_empty() {
        if let Err(err) = scan_dir::run(dirs_to_scan, follow_links) {
            eprintln!("{}", err);
        }
    }
}


#[cfg(test)]
mod tests {
    #[test]
    // A dummy test, for now, just to make sure integration works.
    // Hopefully, this will grow to be a real unit-test someday.
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
