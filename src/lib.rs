/****************************************
 * Copyright (c) 2020 Dedi Hirschfeld
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ****************************************/

//!
/// Scandir app main body.
///

use std::collections::HashSet;
use std::error::Error;
use std::path::PathBuf;
use std::sync::mpsc;
use std::thread;

mod scanner_thread;

/// Number of scanner threads to create.
const NUM_SCANNER_THREADS: usize = 10;

/// A thread descriptor, from the point of view of the manager thread.
struct ThreadDescriptor {
    handle: Option<thread::JoinHandle<()>>,
    tx: mpsc::Sender<scanner_thread::Command>,
}

impl ThreadDescriptor {
    /// Create a new thread descriptor.
    pub fn new(
        handle: thread::JoinHandle<()>,
        tx: mpsc::Sender<scanner_thread::Command>,
    ) -> ThreadDescriptor {
        ThreadDescriptor {
            handle: Some(handle),
            tx: tx,
        }
    }

    pub fn send(
        self: &ThreadDescriptor,
        cmd: scanner_thread::Command,
    ) -> Result<(), Box<dyn Error>> {
        self.tx.send(cmd)?;
        Ok(())
    }

    pub fn join(self: &mut ThreadDescriptor) -> thread::Result<()> {
        if self.handle.is_some() {
            let handle = self.handle.take().unwrap();
            handle.join()?;
        }
        Ok(())
    }
}

// Spawn the scanner threads.
fn spawn_scanners(
    num_scanners: usize,
    tx_to_main: &mpsc::Sender<scanner_thread::Notification>,
    follow_links: bool
) -> Vec<ThreadDescriptor> {
    let mut scanner_threads = vec![];

    for thread in 0..num_scanners {
        let (tx_to_scanner, scanner_rx) = mpsc::channel();
        let tx_to_main = tx_to_main.clone();
        let handle = thread::spawn(move || {
            scanner_thread::run(thread, scanner_rx, tx_to_main, follow_links);
        });
        scanner_threads.push(ThreadDescriptor::new(handle, tx_to_scanner))
    }

    scanner_threads
}

// Tell the scanner threads to finish, and wait for them to be done.
fn join_scanners(mut scanner_threads: Vec<ThreadDescriptor>) -> Result<(), Box<dyn Error>> {
    for thread in scanner_threads.iter() {
        thread.send(scanner_thread::Command::Done)?;
    }

    // TODO: Handle error
    for thread in scanner_threads.iter_mut() {
        thread.join().unwrap();
    }

    Ok(())
}

/// Scan the given directories, by putting the given scanner threads to work.
fn scan_dirs(
    paths: Vec<String>,
    scanner_threads: &Vec<ThreadDescriptor>,
    notification_rx: &mpsc::Receiver<scanner_thread::Notification>,
) -> Result<(), Box<dyn Error>> {
    let mut dirs_to_scan: Vec<PathBuf> = paths.iter().map(|p| PathBuf::from(p)).collect();
    let mut available_scanners = vec![];
    let mut already_visited_dirs = HashSet::new();

    loop {
        match notification_rx.recv()? {
            scanner_thread::Notification::Ready(thread_id) => {
                if let Some(dir_to_scan) = dirs_to_scan.pop() {
                    scanner_threads[thread_id].send(scanner_thread::Command::Scan(dir_to_scan))?;
                } else {
                    // No jobs currently waiting; put this one in the
                    // list of available scanners.
                    available_scanners.push(thread_id);
                    if available_scanners.len() == scanner_threads.len() {
                        // If all threads finished, and no more
                        // directories to scan, we're done.
                        break;
                    }
                }
            }
            scanner_thread::Notification::FoundDir(path) => {
                let canonical_name = path.canonicalize()?;
                if already_visited_dirs.contains(&canonical_name) {
                    continue;
                } else {
                    already_visited_dirs.insert(canonical_name);
                }
                if let Some(scanner_thread) = available_scanners.pop() {
                    scanner_threads[scanner_thread].send(scanner_thread::Command::Scan(path))?;
                } else {
                    // No threads currently available; keep this path
                    // until one is ready to scan it.
                    dirs_to_scan.push(path);
                }
            }
            scanner_thread::Notification::FoundFile(path) => {
                println!("{}", path.to_string_lossy());
            }

            scanner_thread::Notification::Error(err) => {
                eprintln!("{}", err);
            }
        }
    }

    Ok(())
}

/// scan_dir application main body: spawn scanner threads, scan the given
/// paths using them, and wait for them to finish.
pub fn run(paths: Vec<String>, follow_links: bool) -> Result<(), Box<dyn Error>> {
    let (tx_to_main, main_rx) = mpsc::channel();
    let scanner_threads = spawn_scanners(NUM_SCANNER_THREADS, &tx_to_main, follow_links);

    scan_dirs(paths, &scanner_threads, &main_rx)?;

    join_scanners(scanner_threads)?;

    Ok(())
}
