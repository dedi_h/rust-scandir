Rust-scandir
============

What this is
------------
A recursive directory scanner, implemented in Rust.

this is a rather naive implementation, written as an exercise in
writing concurrent Rust code.

How it works
------------

The way this currently works is by launching several
scanner threads, while keeping the main thread as a manager
thread. Manager thread sends 'scan' commands to scanner threads to
scan sub-directories, and scanner threads send back 'Found a file'
and/or 'Found a dir' notifications to the manager.

License
-------

[MIT license](LICENSE). Meaning (as I understand it, IANAL), that you
can do almost anything you want with the code, but you need to keep
the license if you are redistributing all or a substantial part of
it. Also, understand that I'm not responsible if you do something with
this code that causes harm.

Anyway, enjoy.
